defmodule DemoPhoenix.PageController do
  use DemoPhoenix.Web, :controller

  def index(conn, _params) do
    {:ok, url} =
      ExAws.Config.new(:s3)
      |> ExAws.S3.presigned_url(:get, "phoenix", "sample")
    render conn, "index.html", url: url
  end
end
