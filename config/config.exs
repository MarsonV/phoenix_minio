# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :demo_phoenix, DemoPhoenix.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "wz8NmEhvDi/ZQc/G592D6QVLxnwX3MJ8aJhZLS8abEWsSX45YKz+pweDi3D5T41C",
  render_errors: [view: DemoPhoenix.ErrorView, accepts: ~w(html json)],
  pubsub: [name: DemoPhoenix.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
config :ex_aws,
  access_key_id: "SDPZD2A3627HX46NM6UU",
  secret_access_key: "Qw113u/BJFLnGS+yi+NOMcbUhh0gQk81Gkici6D3",
  region: "us-east-1",
  s3: [
    scheme: "http://",
    host: "127.0.0.1:9000",
    region: "us-east-1"
  ]
